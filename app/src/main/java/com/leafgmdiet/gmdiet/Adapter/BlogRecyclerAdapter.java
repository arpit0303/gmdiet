package com.leafgmdiet.gmdiet.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.leafgmdiet.gmdiet.Beans.BlogBean;
import com.leafgmdiet.gmdiet.R;
import com.leafgmdiet.gmdiet.utils.Constant;
import com.leafgmdiet.gmdiet.webactivity.WebActivity;

/**
 * Created by Arpit on 29/03/18.
 */

public class BlogRecyclerAdapter extends RecyclerView.Adapter<BlogRecyclerAdapter.BlogRecyclerViewHolder> {

    Context context;
    BlogBean blogBean;

    public BlogRecyclerAdapter(Context context, BlogBean blogBean) {
        this.context = context;
        this.blogBean = blogBean;
    }

    @Override
    public BlogRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.blog_adapter, null);
        return new BlogRecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BlogRecyclerViewHolder holder, int position) {
        holder.blogTitle.setText(blogBean.getGmDietBlog().get(position).getPostTitle());
        holder.blogSubTitle.setText(blogBean.getGmDietBlog().get(position).getPostContent());
//        Glide.with(context).load(blogBean.getGmDietBlog().get(position).getGuid()).into(holder.blogImage);

        final int mPosition = position;

        holder.blogCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, WebActivity.class);
                intent.putExtra(Constant.WEB_URL, blogBean.getGmDietBlog().get(mPosition).getGuid());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return blogBean.getGmDietBlog().size();
    }

    public class BlogRecyclerViewHolder extends RecyclerView.ViewHolder {

        ImageView blogImage;
        TextView blogTitle, blogSubTitle;
        CardView blogCard;

        public BlogRecyclerViewHolder(View itemView) {
            super(itemView);

            blogCard = itemView.findViewById(R.id.blog_card);
            blogImage = itemView.findViewById(R.id.blog_image);
            blogTitle = itemView.findViewById(R.id.blog_title);
            blogSubTitle = itemView.findViewById(R.id.blog_sub_title);
        }
    }

}
