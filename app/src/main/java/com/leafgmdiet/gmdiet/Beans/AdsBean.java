
package com.leafgmdiet.gmdiet.Beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AdsBean {

    @SerializedName("gm_diet_ads")
    @Expose
    private List<GmDietAd> gmDietAds = null;

    public List<GmDietAd> getGmDietAds() {
        return gmDietAds;
    }

    public void setGmDietAds(List<GmDietAd> gmDietAds) {
        this.gmDietAds = gmDietAds;
    }

}
