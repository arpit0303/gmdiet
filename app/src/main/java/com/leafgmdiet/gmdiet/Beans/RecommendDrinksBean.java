
package com.leafgmdiet.gmdiet.Beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RecommendDrinksBean {

    @SerializedName("gm_diet_drinks")
    @Expose
    private List<GmDietDrink> gmDietDrinks = null;

    public List<GmDietDrink> getGmDietDrinks() {
        return gmDietDrinks;
    }

    public void setGmDietDrinks(List<GmDietDrink> gmDietDrinks) {
        this.gmDietDrinks = gmDietDrinks;
    }

}
