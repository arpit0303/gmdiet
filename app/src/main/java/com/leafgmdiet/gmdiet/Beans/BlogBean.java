
package com.leafgmdiet.gmdiet.Beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BlogBean {

    @SerializedName("gm_diet_blog")
    @Expose
    private List<GmDietBlog> gmDietBlog = null;

    public List<GmDietBlog> getGmDietBlog() {
        return gmDietBlog;
    }

    public void setGmDietBlog(List<GmDietBlog> gmDietBlog) {
        this.gmDietBlog = gmDietBlog;
    }

}
