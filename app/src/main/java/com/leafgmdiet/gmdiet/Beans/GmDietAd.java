
package com.leafgmdiet.gmdiet.Beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GmDietAd {

    @SerializedName("affiliate_url")
    @Expose
    private String affiliateUrl;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("image_url")
    @Expose
    private String imageUrl;

    public String getAffiliateUrl() {
        return affiliateUrl;
    }

    public void setAffiliateUrl(String affiliateUrl) {
        this.affiliateUrl = affiliateUrl;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

}
