package com.leafgmdiet.gmdiet.Beans;

/**
 * Created by Arpit on 03/04/18.
 */

public class UserInfoBean {

    private String userName;
    private String userGender;
    private String userHeight;
    private String userWeight;
    private String userMeal;
    private String userDOB;
    private String userEmail;
    private String userFBToken;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserGender() {
        return userGender;
    }

    public void setUserGender(String userGender) {
        this.userGender = userGender;
    }

    public String getUserHeight() {
        return userHeight;
    }

    public void setUserHeight(String userHeight) {
        this.userHeight = userHeight;
    }

    public String getUserWeight() {
        return userWeight;
    }

    public void setUserWeight(String userWeight) {
        this.userWeight = userWeight;
    }

    public String getUserMeal() {
        return userMeal;
    }

    public void setUserMeal(String userMeal) {
        this.userMeal = userMeal;
    }

    public String getUserDOB() {
        return userDOB;
    }

    public void setUserDOB(String userDOB) {
        this.userDOB = userDOB;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserFBToken() {
        return userFBToken;
    }

    public void setUserFBToken(String userFBToken) {
        this.userFBToken = userFBToken;
    }
}
