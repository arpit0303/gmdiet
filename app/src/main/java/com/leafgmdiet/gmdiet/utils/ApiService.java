package com.leafgmdiet.gmdiet.utils;

import com.leafgmdiet.gmdiet.Beans.AdsBean;
import com.leafgmdiet.gmdiet.Beans.BlogBean;
import com.leafgmdiet.gmdiet.Beans.OutputBean;
import com.leafgmdiet.gmdiet.Beans.RecommendDrinksBean;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by Arpit on 01/04/18.
 */

public interface ApiService {

@GET("gm_diet_drinks")
Call<RecommendDrinksBean> getRecommendDrinks();

@POST("gm_diet_user")
Call<OutputBean> postUserInfo(@Body HashMap<String, String> body);

@GET("gm_diet_ads")
    Call<AdsBean> getAds();

@GET("gm_diet_blog")
    Call<BlogBean> getBlogPosts();
}
