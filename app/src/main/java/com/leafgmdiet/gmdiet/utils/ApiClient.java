package com.leafgmdiet.gmdiet.utils;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Arpit on 01/04/18.
 */

public class ApiClient{

    private static final String BASE_URL = "http://4164be60.ngrok.io/";

    public static Retrofit getClient() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit;
    }
}
