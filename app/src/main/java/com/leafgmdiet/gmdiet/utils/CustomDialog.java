package com.leafgmdiet.gmdiet.utils;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;

import com.leafgmdiet.gmdiet.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Arpit on 15/03/18.
 */

public class CustomDialog extends Dialog implements View.OnClickListener{

    @BindView(R.id.dialog_cancel)
    ImageView dialogCancel;
    Context context;

    CustomDialog dialog;

    public CustomDialog(@NonNull Context context) {
        super(context);
        this.context = context;
    } 

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom_dialog);
        ButterKnife.bind(this);

        init();
    }

    private void init() {
        dialogCancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.dialog_cancel:
                    onBackPressed();
                break;
        }
    }
}
