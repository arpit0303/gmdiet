package com.leafgmdiet.gmdiet.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Arpit on 30/01/18.
 */

public class SharedPreferenceUtils {

    private static final String USER_DATA = "user_data";
    private static final String IS_LOGIN = "isLoggedIn";
    private static final String USER_GENDER = "user_gender";
    private static final String USER_WEIGHT = "user_weight";
    private static final String USER_HEIGHT = "user_height";
    private static final String DIET_OPTION = "diet_option";
    private static final String MEAL_OPTION = "meal_option";
    private static final String USER_JSON_DATA = "user_json_data";

    public static String getUserMeal(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(USER_DATA, Context.MODE_PRIVATE);
        return sharedPreferences.getString(MEAL_OPTION, null);
    }

    public static void setUserMeal(Context context, String mealOption) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(USER_DATA, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(MEAL_OPTION, mealOption);
        editor.apply();
    }

    public static boolean isLoggedIn(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(USER_DATA, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(IS_LOGIN, false);
    }

    public static void setLoginStatus(Context context, boolean isLogin) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(USER_DATA, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(IS_LOGIN, isLogin);
        editor.apply();
    }

    public static String getUserGender(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(USER_DATA, Context.MODE_PRIVATE);
        return sharedPreferences.getString(USER_GENDER, null);
    }

    public static void setUserGender(Context context, String gender) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(USER_DATA, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(USER_GENDER, gender);
        editor.apply();
    }

    public static String getUserWeight(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(USER_DATA, Context.MODE_PRIVATE);
        return sharedPreferences.getString(USER_WEIGHT, null);
    }

    public static void setUserWeight(Context context, String weight) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(USER_DATA, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(USER_WEIGHT, weight);
        editor.apply();
    }

    public static String getUserHeight(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(USER_DATA, Context.MODE_PRIVATE);
        return sharedPreferences.getString(USER_HEIGHT, null);
    }

    public static void setUserHeight(Context context, String height) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(USER_DATA, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(USER_HEIGHT, height);
        editor.apply();
    }

    public static String getUserDiet(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(USER_DATA, Context.MODE_PRIVATE);
        return sharedPreferences.getString(DIET_OPTION, null);
    }

    public static void setUserDiet(Context context, String diet) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(USER_DATA, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(DIET_OPTION, diet);
        editor.apply();
    }

    public static String getUserData(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(USER_DATA, Context.MODE_PRIVATE);
        return sharedPreferences.getString(USER_JSON_DATA, null);
    }

    public static void setUserData(Context context, String data) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(USER_DATA, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(USER_JSON_DATA, data);
        editor.apply();
    }
}
