package com.leafgmdiet.gmdiet.utils;

/**
 * Created by Arpit on 31/01/18.
 */

public class Constant {

    public static final String OPTION_VEG = "1";

    public static final String OPTION_NON_VEG = "2";

    public static final String KEY_DAY = "day";

    public static final int CONTENT_PAGE_COUNT = 3;

    public static final String DAY_1 = "day1";

    public static final String DAY_2 = "day2";

    public static final String DAY_3 = "day3";

    public static final String DAY_4 = "day4";

    public static final String DAY_5 = "day5";

    public static final String DAY_6 = "day6";

    public static final String DAY_7 = "day7";

    public static final String INTENT_JSON_DATA = "intent_json_data";

    public static final String WEB_URL = "web_url";

    public static final int TIME_INTERVAL = 5000;
}
