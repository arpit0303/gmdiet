package com.leafgmdiet.gmdiet.content;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.leafgmdiet.gmdiet.R;
import com.leafgmdiet.gmdiet.drinkFragment.DrinkFragment;
import com.leafgmdiet.gmdiet.exerciseFragment.ExerciseFragment;
import com.leafgmdiet.gmdiet.foodFragment.FoodFragment;
import com.leafgmdiet.gmdiet.utils.Constant;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ContentActivity extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    private String dayContent;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.view_pager) ViewPager mViewPager;
    @BindView(R.id.tabs) TabLayout tabLayout;
    @BindView(R.id.back_image) ImageView backImage;
    @BindView(R.id.toolbar_title) TextView pageTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content);
        ButterKnife.bind(this);

        initToolBar();
        init();
    }

    private void init() {

        dayContent = getIntent().getStringExtra(Constant.KEY_DAY);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager.setAdapter(mSectionsPagerAdapter);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));
    }

    private void initToolBar() {
        setSupportActionBar(toolbar);
        pageTitle.setText(getIntent().getStringExtra(Constant.KEY_DAY));
    }

    @OnClick(R.id.back_image)
    public void backPressed() {
        onBackPressed();
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            switch (position) {
                case 0:
                    return FoodFragment.getInstance(dayContent);
                case 1:
                    return DrinkFragment.getInstance(getIntent().getStringExtra(Constant.INTENT_JSON_DATA));
                case 2:
                    return ExerciseFragment.getInstance();
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return Constant.CONTENT_PAGE_COUNT;
        }
    }
}
