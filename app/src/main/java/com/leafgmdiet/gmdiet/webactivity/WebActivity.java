package com.leafgmdiet.gmdiet.webactivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;

import com.leafgmdiet.gmdiet.R;
import com.leafgmdiet.gmdiet.utils.Constant;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WebActivity extends AppCompatActivity {

    @BindView(R.id.webView)
    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);
        ButterKnife.bind(this);

        if(getIntent() != null) {
            init(getIntent().getStringExtra(Constant.WEB_URL));
        }else {
            finish();
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void init(String url) {
        webView.getSettings().setJavaScriptEnabled(true);

        webView.loadUrl(url);
    }
}
