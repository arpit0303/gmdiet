package com.leafgmdiet.gmdiet.Login;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.google.gson.Gson;
import com.leafgmdiet.gmdiet.Beans.UserInfoBean;
import com.leafgmdiet.gmdiet.InitInput.InitInputActivity;
import com.leafgmdiet.gmdiet.R;
import com.leafgmdiet.gmdiet.utils.Constant;
import com.leafgmdiet.gmdiet.utils.SharedPreferenceUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity implements LoginContract.View {

    @BindView(R.id.terms_text)
    TextView termsText;
    @BindView(R.id.policy_text)
    TextView policyText;
    @BindView(R.id.fb_login)
    RelativeLayout fbLogin;

    LoginPresenter presenter;
    CallbackManager callbackManager;

    UserInfoBean userInfoBean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        initializeBean();
        init();
    }

    private void initializeBean() {
        userInfoBean = new UserInfoBean();
    }

    private void init() {
        presenter = new LoginPresenter(this, userInfoBean);
        callbackManager = CallbackManager.Factory.create();
    }

    @OnClick(R.id.terms_text)
    public void onTermClick() {

    }

    @OnClick(R.id.policy_text)
    public void onPolicyClick() {

    }

    @OnClick(R.id.fb_login)
    public void onFBClick() {
        presenter.fbLogin(LoginActivity.this, callbackManager);
//        onFBLoginSuccess();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onFBLoginSuccess() {
        Log.i("hello", new Gson().toJson(userInfoBean));
        SharedPreferenceUtils.setLoginStatus(LoginActivity.this, true);
        SharedPreferenceUtils.setUserData(LoginActivity.this, new Gson().toJson(userInfoBean));
        Intent intent = new Intent(LoginActivity.this, InitInputActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(Constant.INTENT_JSON_DATA, new Gson().toJson(userInfoBean));
        startActivity(intent);
        finish();
    }
}
