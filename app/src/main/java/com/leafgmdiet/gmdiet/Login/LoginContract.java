package com.leafgmdiet.gmdiet.Login;

import android.app.Activity;

import com.facebook.CallbackManager;

/**
 * Created by Arpit on 26/12/17.
 */

interface LoginContract {

    public interface Presenter {
        void fbLogin(Activity activity, CallbackManager callbackManager);
    }

    public interface View {
        void onFBLoginSuccess();
    }
}
