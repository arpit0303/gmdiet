package com.leafgmdiet.gmdiet.Login;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.gson.Gson;
import com.leafgmdiet.gmdiet.Beans.UserInfoBean;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

/**
 * Created by Arpit on 25/12/17.
 */

public class LoginPresenter implements LoginContract.Presenter{

    LoginActivity view;
    UserInfoBean userInfoBean;

    public LoginPresenter(LoginActivity context, UserInfoBean userInfoBean) {
        view = context;
        this.userInfoBean = userInfoBean;
    }

    @Override
    public void fbLogin(final Activity activity, CallbackManager callbackManager) {
        LoginManager.getInstance().logInWithReadPermissions(activity, Arrays.asList("public_profile"));

        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.i("MainActivity", "@@@onSuccess");
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.i("MainActivity", "@@@response: " + response.toString());
                                Log.i("MainActivity", "@@@object: " + new Gson().toJson(object));

                                try {
                                    userInfoBean.setUserFBToken(object.getString("id"));

                                    if(object.getString("name") != null) {
                                        userInfoBean.setUserName(object.getString("name"));
                                    }

                                    if(object.getString("birthday") != null) {
                                        userInfoBean.setUserDOB(object.getString("dob"));
                                    }

                                    if(object.getString("email") != null) {
                                        userInfoBean.setUserEmail(object.getString("email"));
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                view.onFBLoginSuccess();
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });
    }
}
