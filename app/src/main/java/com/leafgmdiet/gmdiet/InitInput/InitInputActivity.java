package com.leafgmdiet.gmdiet.InitInput;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.h6ah4i.android.widget.verticalseekbar.VerticalSeekBar;
import com.leafgmdiet.gmdiet.Beans.UserInfoBean;
import com.leafgmdiet.gmdiet.Home.HomeActivity;
import com.leafgmdiet.gmdiet.R;
import com.leafgmdiet.gmdiet.utils.AppUtils;
import com.leafgmdiet.gmdiet.utils.Constant;
import com.leafgmdiet.gmdiet.utils.SharedPreferenceUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class InitInputActivity extends AppCompatActivity implements InitInputContract.InitInputView {

    @BindView(R.id.male_card)
    CardView maleCard;
    @BindView(R.id.female_card)
    CardView femaleCard;
    @BindView(R.id.next)
    ImageView next;
    @BindView(R.id.text_male)
    TextView maleText;
    @BindView(R.id.text_female)
    TextView femaleText;
    @BindView(R.id.male_circle_image)
    ImageView maleCircleImage;
    @BindView(R.id.female_circle_image)
    ImageView femaleCircleImage;
    @BindView(R.id.gender_layout)
    LinearLayout genderLayout;
    @BindView(R.id.h_w_layout)
    LinearLayout heightWeightLayout;
    @BindView(R.id.done)
    ImageView done;
    @BindView(R.id.height_SeekBar)
    VerticalSeekBar heightSeekBar;
    @BindView(R.id.weight_SeekBar)
    VerticalSeekBar weightSeekBar;
    @BindView(R.id.meal_layout) LinearLayout mealLayout;
    @BindView(R.id.meal_next) ImageView mealNext;
    @BindView(R.id.veg_meal_card) CardView vegMealCard;
    @BindView(R.id.nonveg_meal_card) CardView nonvegMealCard;
    @BindView(R.id.text_veg) TextView textVeg;
    @BindView(R.id.text_nonveg) TextView textNonVeg;

    InitInputPresenter presenter;

    UserInfoBean userInfoBean;

    int heightValue = 0;
    int weightValue = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_init_input);
        ButterKnife.bind(this);

        initializeBean();
        initPresenter();
        init();
    }

    private void initializeBean() {
        if(getIntent().getStringExtra(Constant.INTENT_JSON_DATA) != null) {
            userInfoBean = new Gson().fromJson(getIntent().getStringExtra(Constant.INTENT_JSON_DATA), UserInfoBean.class);
        }else{
            userInfoBean = new Gson().fromJson(SharedPreferenceUtils.getUserData(InitInputActivity.this), UserInfoBean.class);
        }
    }

    private void init() {
        genderLayout.setVisibility(View.VISIBLE);
        mealLayout.setVisibility(View.GONE);
        heightWeightLayout.setVisibility(View.GONE);

        heightSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                heightValue = i * 10;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        weightSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                weightValue = i * 10;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void initPresenter() {
        presenter = new InitInputPresenter(this, userInfoBean);
    }

    @OnClick(R.id.next)
    public void gotoNext() {
        if(userInfoBean.getUserGender() != null) {
            genderLayout.setVisibility(View.GONE);
            mealLayout.setVisibility(View.VISIBLE);
        }else {
            Toast.makeText(this, "Please select the gender.", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.meal_next)
    public void gotoMealNext() {
        if(userInfoBean.getUserMeal() != null) {
            mealLayout.setVisibility(View.GONE);
            heightWeightLayout.setVisibility(View.VISIBLE);
        }else {
            Toast.makeText(this, "Please select any meal option", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.veg_meal_card)
    public void vegMealCardPressed() {
        userInfoBean.setUserMeal("veg");

        SharedPreferenceUtils.setUserMeal(InitInputActivity.this, "veg");
        textVeg.setTextColor(getResources().getColor(R.color.white));
        textVeg.setBackgroundColor(getResources().getColor(R.color.dark_blue));

        textNonVeg.setTextColor(getResources().getColor(R.color.gray));
        textNonVeg.setBackgroundColor(getResources().getColor(R.color.white));
    }

    @OnClick(R.id.nonveg_meal_card)
    public void nonVegMealCardPressed() {
        userInfoBean.setUserMeal("nonveg");

        SharedPreferenceUtils.setUserMeal(InitInputActivity.this, "nonveg");
        textNonVeg.setTextColor(getResources().getColor(R.color.white));
        textNonVeg.setBackgroundColor(getResources().getColor(R.color.dark_blue));

        textVeg.setTextColor(getResources().getColor(R.color.gray));
        textVeg.setBackgroundColor(getResources().getColor(R.color.white));
    }

    @OnClick(R.id.female_card)
    public void femaleCardPressed() {
        userInfoBean.setUserGender("female");

        SharedPreferenceUtils.setUserGender(InitInputActivity.this, "female");
        femaleCircleImage.setVisibility(View.VISIBLE);
        femaleText.setTextColor(getResources().getColor(android.R.color.white));

        maleCircleImage.setVisibility(View.GONE);
        maleText.setTextColor(getResources().getColor(android.R.color.black));
    }

    @OnClick(R.id.male_card)
    public void maleCardPressed() {
        userInfoBean.setUserGender("male");

        SharedPreferenceUtils.setUserGender(InitInputActivity.this, "male");
        maleCircleImage.setVisibility(View.VISIBLE);
        maleText.setTextColor(getResources().getColor(android.R.color.white));

        femaleCircleImage.setVisibility(View.GONE);
        femaleText.setTextColor(getResources().getColor(android.R.color.black));
    }

    @OnClick(R.id.done)
    public void doneClicked() {
        if(weightValue != 0 && heightValue != 0) {
            SharedPreferenceUtils.setUserWeight(InitInputActivity.this, "" + weightValue);
            SharedPreferenceUtils.setUserHeight(InitInputActivity.this, "" + heightValue);

            userInfoBean.setUserHeight("" + heightValue);
            userInfoBean.setUserWeight("" + weightValue);

//            userInfoBean.setUserName("xyz");
//            userInfoBean.setUserDOB("01/01/1992");
//            userInfoBean.setUserEmail("xyz@gmail.com");

            AppUtils.showProgressDialog(InitInputActivity.this, getResources().getString(R.string.fetching_details));
            presenter.postUserInfo();

//            postUserInfoSuccess("success");
        }else {
            Toast.makeText(this, "Please select the weight and height.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void postUserInfoSuccess(String message) {
        AppUtils.dismissProgressDialog();

        if(message.equalsIgnoreCase("success")) {
            Intent intent = new Intent(InitInputActivity.this, HomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }else {
            AppUtils.showToast(this, getResources().getString(R.string.fetching_error));
        }
    }

    @Override
    public void postUserInfoFailure() {
        AppUtils.dismissProgressDialog();
        Toast.makeText(InitInputActivity.this, getResources().getString(R.string.fetching_error), Toast.LENGTH_SHORT).show();
    }
}
