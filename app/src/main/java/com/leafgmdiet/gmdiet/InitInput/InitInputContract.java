package com.leafgmdiet.gmdiet.InitInput;

/**
 * Created by Arpit on 30/12/17.
 */

public interface InitInputContract {

    public interface InitInputView {
        void postUserInfoSuccess(String message);

        void postUserInfoFailure();
    }

    public interface InitInputPresenter {
        void postUserInfo();
    }
}
