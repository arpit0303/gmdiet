package com.leafgmdiet.gmdiet.InitInput;

import android.util.Log;

import com.google.gson.Gson;
import com.leafgmdiet.gmdiet.Beans.OutputBean;
import com.leafgmdiet.gmdiet.Beans.UserInfoBean;
import com.leafgmdiet.gmdiet.utils.ApiClient;
import com.leafgmdiet.gmdiet.utils.ApiService;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Arpit on 30/12/17.
 */

public class InitInputPresenter implements InitInputContract.InitInputPresenter {

    private InitInputContract.InitInputView view;
    private UserInfoBean userInfoBean;

    public InitInputPresenter(InitInputContract.InitInputView view, UserInfoBean userInfoBean) {
        this.view = view;
        this.userInfoBean = userInfoBean;
    }

    @Override
    public void postUserInfo() {
        ApiService apiService = ApiClient.getClient().create(ApiService.class);
        Call<OutputBean> userInfo = apiService.postUserInfo(getUserInfo());

        userInfo.enqueue(new Callback<OutputBean>() {
            @Override
            public void onResponse(Call<OutputBean> call, Response<OutputBean> response) {
                Log.i("Hello", "UserData: "+ new Gson().toJson(response.body()));
                if(response.body() != null) {
                    view.postUserInfoSuccess(response.body().getMessage());
                }else {
                    view.postUserInfoFailure();
                }
            }

            @Override
            public void onFailure(Call<OutputBean> call, Throwable t) {
                view.postUserInfoFailure();
            }
        });
    }

//    {
//        "user_name":"",
//            "gender":"male/female",
//            "height":"112",
//            "weight":"60.2",
//            "meal":"veg/nonveg",
//            "dob":"",
//            "email":"",
//            "fb_token":"sds"
//    }

    private HashMap<String, String> getUserInfo() {
        HashMap<String, String> info = new HashMap<>();
        info.put("user_name", userInfoBean.getUserName());
        info.put("fb_token", userInfoBean.getUserFBToken());
        info.put("weight", userInfoBean.getUserWeight());
        info.put("gender", userInfoBean.getUserGender());
        info.put("height", userInfoBean.getUserHeight());
        info.put("meal", userInfoBean.getUserMeal());
        info.put("dob", "" +userInfoBean.getUserDOB());
        info.put("email", "" + userInfoBean.getUserEmail());

        Log.i("hello", "userInfo: "+ new Gson().toJson(info));

        return info;
    }
}
