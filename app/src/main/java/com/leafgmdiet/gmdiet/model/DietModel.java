package com.leafgmdiet.gmdiet.model;

import com.leafgmdiet.gmdiet.R;

/**
 * Created by Arpit on 14/03/18.
 */

public class DietModel {

    public static final String[] FOOD_TIMING = {"8 AM", "11 AM", "1:30 PM", "4 PM", "6:30 PM", "8 PM"};

    public static final String[] FOOD_TYPE = {"Breakfast", "Brunch", "Lunch", "Post Lunch Snack", "Evening Snack", "Dinner"};

    public static final String[] DAY1_FOOD = {
            "A regular sized apple with a glass of water",
            "Half a bowl of sliced cantaloupe with a glass of water",
            "A serving of sliced watermelon and two glasses of water",
            "A whole orange and a glass of water",
            "An apple and a glass of water",
            "Sliced cantaloupe and a guava with two glasses of water"
    };

    public static final String[] DAY2_FOOD = {
            "A boiled potato. You can also add a teaspoon of low-fat butter for flavor",
            "Cabbage and lettuce salad with a light dressing and a glass of water",
            "A mixed vegetable salad with cucumbers, onions, and carrots and two glasses of water",
            "A cup of boiled broccoli, half a cup of sliced bell pepper, and two glasses of water",
            "Some boiled cauliflower with a light dressing and a glass of water",
            "A salad comprising boiled carrots, broccoli, and green beans, and two glasses of water"
    };

    public static final String[] DAY3_FOOD = {
            "Half a bowl of cantaloupe or a sliced apple and two glasses of water",
            "Half a sliced pineapple or a pear and two glasses of water",
            "A salad of cucumber, carrots, and lettuce with two glasses of water",
            "An orange with half a sliced cantaloupe and a glass of water",
            "A pear and a glass of water",
            "Boiled broccoli and beets with two glasses of water"
    };

    public static final String[] DAY4_FOOD = {
            "Two large bananas and a glass of milk",
            "A banana shake (use one banana). Half a teaspoon of honey can be added as a sweetener",
            "A bowl of GM diet soup",
            "Banana milkshake",
            "Two medium to small bananas",
            "Two large bananas and a glass of milk"
    };

    public static final String[] DAY5_FOOD = {
            "A few small tomatoes with a bowl of boiled, seasoned kidney beans and two glasses of water",
            "A cup of yogurt and two glasses of water",
            "A bowl of cooked brown rice/chicken breast/fish with two tomatoes and two glasses of water",
            "A salad with onions, and sprouts with two glasses of water",
            "An apple and a pear",
            "GM Diet soup with two glasses of water"
    };

    public static final String[] DAY6_FOOD = {
            "A bowl of mixed boiled vegetables and two glasses of water",
            "A bowl of boiled kidney beans with a diced tomato seasoned with spices, and two glasses of water",
            "A bowl of brown rice/chicken breast/fish with the GM diet soup and two glasses of water",
            "3-4 baby carrots and a glass of water",
            "A bowl of GM soup and a glass of water",
            "A bowl of boiled vegetables with a glass of water"
    };

    public static final String[] DAY7_FOOD = {
            "A mixed vegetable salad in a small bowl and a glass of apple or orange juice",
            "A bowl of GM diet soup and a small portion of carrot sticks with a glass of water",
            "A cup of brown rice, a bowl of boiled vegetables, and two glasses of water",
            "A few carrots and a glass of kiwi juice",
            "A mixed vegetable salad and two glasses of water",
            "A bowl of GM diet soup and two glasses of water"
    };

    public static final String[] EXERCISE_TITLE = {"Arm circles", "Wrist circles", "Ankle rotation", "Leg rotation", "Waist rotation", "Waist rotation"};

    public static final int[] EXERCISE_IMAGE = {R.drawable.apple, R.drawable.apple, R.drawable.apple, R.drawable.apple, R.drawable.apple, R.drawable.apple};
}

