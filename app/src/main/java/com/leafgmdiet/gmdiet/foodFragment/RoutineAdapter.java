package com.leafgmdiet.gmdiet.foodFragment;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.leafgmdiet.gmdiet.R;
import com.leafgmdiet.gmdiet.model.DietModel;
import com.leafgmdiet.gmdiet.utils.Constant;

/**
 * Created by Arpit on 01/02/18.
 */

public class RoutineAdapter extends RecyclerView.Adapter<RoutineAdapter.RoutineViewHolder>{

    Context context;
    String[] selectedFood;

    public RoutineAdapter(Context context, String selectedDay) {
        this.context = context;
        init(selectedDay);
    }

    public void init(String selectedDay) {
        selectedFood = new String[6];
        if (selectedDay.equalsIgnoreCase(Constant.DAY_1)) {
            selectedFood = DietModel.DAY1_FOOD;
        } else if (selectedDay.equalsIgnoreCase(Constant.DAY_2)) {
            selectedFood = DietModel.DAY2_FOOD;
        } else if (selectedDay.equalsIgnoreCase(Constant.DAY_3)) {
            selectedFood = DietModel.DAY3_FOOD;
        }else if(selectedDay.equalsIgnoreCase(Constant.DAY_4)) {
            selectedFood = DietModel.DAY4_FOOD;
        }else if(selectedDay.equalsIgnoreCase(Constant.DAY_5)) {
            selectedFood = DietModel.DAY5_FOOD;
        }else if(selectedDay.equalsIgnoreCase(Constant.DAY_6)) {
            selectedFood = DietModel.DAY6_FOOD;
        }else if(selectedDay.equalsIgnoreCase(Constant.DAY_7)) {
            selectedFood = DietModel.DAY7_FOOD;
        }
    }

    @Override
    public RoutineViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_routine, null);
        return new RoutineViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RoutineViewHolder holder, int position) {
        holder.tvFoodTiming.setText(DietModel.FOOD_TIMING[position]);
        holder.tvFoodType.setText(DietModel.FOOD_TYPE[position]);
        holder.tvFoodDesc.setText(selectedFood[position]);
    }

    @Override
    public int getItemCount() {
        return DietModel.FOOD_TIMING.length;
    }

    public class RoutineViewHolder extends RecyclerView.ViewHolder {

        TextView tvFoodTiming, tvFoodType, tvFoodDesc;

        public RoutineViewHolder(View itemView) {
            super(itemView);

            tvFoodTiming = itemView.findViewById(R.id.foodTiming);
            tvFoodType = itemView.findViewById(R.id.food_type);
            tvFoodDesc = itemView.findViewById(R.id.food_desc);
        }
    }
}