package com.leafgmdiet.gmdiet.foodFragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.leafgmdiet.gmdiet.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Arpit on 31/01/18.
 */

public class FoodFragment extends Fragment {

    @BindView(R.id.food_options_recycler_view)
    RecyclerView foodOptionsRecyclerView;

    @BindView(R.id.routine_recycler_view)
    RecyclerView routineRecyclerView;

    static String daySelected;

    public static FoodFragment getInstance(String dayContent) {
        FoodFragment foodFragment = new FoodFragment();
        daySelected = dayContent;
        return foodFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_food, container, false);
        ButterKnife.bind(this, rootView);
        init();

        return rootView;
    }

    private void init() {
        FoodOptionsAdapter foodOptionsAdapter = new FoodOptionsAdapter(getActivity());
        foodOptionsRecyclerView.setAdapter(foodOptionsAdapter);
        foodOptionsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));

        RoutineAdapter routineAdapter = new RoutineAdapter(getActivity(), daySelected);
        routineRecyclerView.setAdapter(routineAdapter);
        routineRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    }
}
