package com.leafgmdiet.gmdiet.foodFragment;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.leafgmdiet.gmdiet.R;
import com.leafgmdiet.gmdiet.utils.CustomDialog;

/**
 * Created by Arpit on 01/02/18.
 */

public class FoodOptionsAdapter extends RecyclerView.Adapter<FoodOptionsAdapter.FoodOptionsViewHolder> implements View.OnClickListener {

    Context context;

    public FoodOptionsAdapter(Context context) {
        this.context = context;
    }

    @Override
    public FoodOptionsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_food_options, null);
        return new FoodOptionsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FoodOptionsViewHolder holder, int position) {
        holder.llSuggestedFood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CustomDialog customDialog = new CustomDialog(context);
                customDialog.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return 8;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ll_suggested_food:
                CustomDialog customDialog = new CustomDialog(context);
                customDialog.show();
                break;
        }
    }

    public class FoodOptionsViewHolder extends RecyclerView.ViewHolder {

        LinearLayout llSuggestedFood;

        public FoodOptionsViewHolder(View itemView) {
            super(itemView);

            llSuggestedFood = itemView.findViewById(R.id.ll_suggested_food);
        }
    }
}