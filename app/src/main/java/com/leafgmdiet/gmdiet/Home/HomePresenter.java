package com.leafgmdiet.gmdiet.Home;

import android.content.Context;
import android.content.Intent;

import com.google.gson.Gson;
import com.leafgmdiet.gmdiet.Beans.AdsBean;
import com.leafgmdiet.gmdiet.Beans.BlogBean;
import com.leafgmdiet.gmdiet.Beans.RecommendDrinksBean;
import com.leafgmdiet.gmdiet.InitInput.InitInputActivity;
import com.leafgmdiet.gmdiet.Login.LoginActivity;
import com.leafgmdiet.gmdiet.utils.ApiClient;
import com.leafgmdiet.gmdiet.utils.ApiService;
import com.leafgmdiet.gmdiet.utils.SharedPreferenceUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Arpit on 25/12/17.
 */

public class HomePresenter implements HomeContract.Presenter{

    private HomeContract.View view;
    private RecommendDrinksBean bean;
    private AdsBean adsBean;
    private BlogBean blogBean;

    public HomePresenter(HomeContract.View view, RecommendDrinksBean bean, AdsBean adsBean, BlogBean blogBean) {
        this.view = view;
        this.bean = bean;
        this.adsBean = adsBean;
        this.blogBean = blogBean;
    }

    @Override
    public boolean checkLoginAndUserData(Context context) {
        if (!SharedPreferenceUtils.isLoggedIn(context)) {
            Intent intent = new Intent(context, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
            return true;
        }else if(SharedPreferenceUtils.getUserGender(context) == null ||
                SharedPreferenceUtils.getUserHeight(context) == null ||
                SharedPreferenceUtils.getUserWeight(context) == null) {
            Intent intent = new Intent(context, InitInputActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
            return true;
        }else {
            return false;
        }
    }

    @Override
    public void fetchRecommendDrinks(final Context context) {
        ApiService apiService = ApiClient.getClient().create(ApiService.class);
        Call<RecommendDrinksBean> drinksList = apiService.getRecommendDrinks();

        drinksList.enqueue(new Callback<RecommendDrinksBean>() {
            @Override
            public void onResponse(Call<RecommendDrinksBean> call, Response<RecommendDrinksBean> response) {
                if(response.body() != null) {
                    String outputString = new Gson().toJson(response.body());
                    RecommendDrinksBean recommendDrinksBean = new Gson().fromJson(outputString, RecommendDrinksBean.class);

                    bean.setGmDietDrinks(recommendDrinksBean.getGmDietDrinks());

                    view.recommendDrinksSuccess();
                }else {
                    view.recommendDrinksFailure();
                }
            }

            @Override
            public void onFailure(Call<RecommendDrinksBean> call, Throwable t) {
                view.recommendDrinksFailure();
            }
        });
    }

    @Override
    public void fetchAds() {
        ApiService apiService = ApiClient.getClient().create(ApiService.class);
        Call<AdsBean> drinksList = apiService.getAds();

        drinksList.enqueue(new Callback<AdsBean>() {
            @Override
            public void onResponse(Call<AdsBean> call, Response<AdsBean> response) {
                if(response.body() != null) {
                    String outputString = new Gson().toJson(response.body());
                    AdsBean mAdsBean = new Gson().fromJson(outputString, AdsBean.class);

                    adsBean.setGmDietAds(mAdsBean.getGmDietAds());

                    view.fetchingAdsSuccess();
                }else {
                    view.fetchingAdsFailure();
                }
            }

            @Override
            public void onFailure(Call<AdsBean> call, Throwable t) {
                view.fetchingAdsFailure();
            }
        });
    }

    @Override
    public void fetchBlogPosts() {
        ApiService apiService = ApiClient.getClient().create(ApiService.class);
        Call<BlogBean> drinksList = apiService.getBlogPosts();

        drinksList.enqueue(new Callback<BlogBean>() {
            @Override
            public void onResponse(Call<BlogBean> call, Response<BlogBean> response) {
                if(response.body() != null) {
                    String outputString = new Gson().toJson(response.body());
                    BlogBean mBlogBean = new Gson().fromJson(outputString, BlogBean.class);

                    blogBean.setGmDietBlog(mBlogBean.getGmDietBlog());

                    view.fetchingBlogPostsSuccess();
                }else {
                    view.fetchingBlogPostsFailure();
                }
            }

            @Override
            public void onFailure(Call<BlogBean> call, Throwable t) {
                view.fetchingBlogPostsFailure();
            }
        });
    }


}
