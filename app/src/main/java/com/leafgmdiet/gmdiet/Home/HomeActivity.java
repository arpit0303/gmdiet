package com.leafgmdiet.gmdiet.Home;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.leafgmdiet.gmdiet.Adapter.BlogRecyclerAdapter;
import com.leafgmdiet.gmdiet.Beans.AdsBean;
import com.leafgmdiet.gmdiet.Beans.BlogBean;
import com.leafgmdiet.gmdiet.Beans.RecommendDrinksBean;
import com.leafgmdiet.gmdiet.R;
import com.leafgmdiet.gmdiet.content.ContentActivity;
import com.leafgmdiet.gmdiet.utils.AppUtils;
import com.leafgmdiet.gmdiet.utils.Constant;
import com.leafgmdiet.gmdiet.webactivity.WebActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeActivity extends AppCompatActivity implements HomeContract.View{

    @BindView(R.id.day1_btn) Button buttonDay1;
    @BindView(R.id.day2_btn) Button buttonDay2;
    @BindView(R.id.day3_btn) Button buttonDay3;
    @BindView(R.id.day4_btn) Button buttonDay4;
    @BindView(R.id.day5_btn) Button buttonDay5;
    @BindView(R.id.day6_btn) Button buttonDay6;
    @BindView(R.id.day7_btn) Button buttonDay7;
//    @BindView(R.id.options_veg) TextView optionVeg;
//    @BindView(R.id.options_non_veg) TextView optionNonVeg;
    @BindView(R.id.weight_yourself_btn)
    RelativeLayout weightYourself;

    @BindView(R.id.ads_image)
    ImageView adsImage;

    @BindView(R.id.blog_recyclerview)
    RecyclerView blogRecyclerView;

    HomePresenter presenter;
    RecommendDrinksBean bean;

    AdsBean adsBean;
    BlogBean blogBean;

    CountDownTimer countDownTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        initBean();
        initPresenter();
        initialization();
    }

    private void initialization() {
        if(presenter.checkLoginAndUserData(HomeActivity.this)) {
            finish();
        }else {
            AppUtils.showProgressDialog(HomeActivity.this, getResources().getString(R.string.fetching_details));
            presenter.fetchRecommendDrinks(HomeActivity.this);
        }
    }

    private void initPresenter() {
        presenter = new HomePresenter(this, bean, adsBean, blogBean);
    }

    private void initBean() {
        bean = new RecommendDrinksBean();
        adsBean = new AdsBean();
        blogBean = new BlogBean();
    }

    @OnClick(R.id.day1_btn)
    public void onClickDay1() {
        gotoPage(Constant.DAY_1);
    }

    @OnClick(R.id.day2_btn)
    public void onClickDay2() {
        gotoPage(Constant.DAY_2);
    }

    @OnClick(R.id.day3_btn)
    public void onClickDay3() {
        gotoPage(Constant.DAY_3);
    }

    @OnClick(R.id.day4_btn)
    public void onClickDay4() {
        gotoPage(Constant.DAY_4);
    }

    @OnClick(R.id.day5_btn)
    public void onClickDay5() {
        gotoPage(Constant.DAY_5);
    }

    @OnClick(R.id.day6_btn)
    public void onClickDay6() {
        gotoPage(Constant.DAY_6);
    }

    @OnClick(R.id.day7_btn)
    public void onClickDay7() {
        gotoPage(Constant.DAY_7);
    }

    @OnClick(R.id.weight_yourself_btn)
    public void onClickWeight() {
        
    }

    @OnClick
    public void onAdClick(){
        Intent intent = new Intent(HomeActivity.this, WebActivity.class);
        intent.putExtra(Constant.WEB_URL, adsBean.getGmDietAds().get(0).getAffiliateUrl());
        startActivity(intent);
    }

    public void gotoPage(String day) {
        Intent intent = new Intent(HomeActivity.this, ContentActivity.class);
        intent.putExtra(Constant.KEY_DAY, day);
        intent.putExtra(Constant.INTENT_JSON_DATA, new Gson().toJson(bean));
        startActivity(intent);
    }

    @Override
    public void recommendDrinksSuccess() {
        presenter.fetchAds();
    }

    @Override
    public void recommendDrinksFailure() {
        AppUtils.dismissProgressDialog();
        Toast.makeText(this, getResources().getString(R.string.fetching_error), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void fetchingAdsSuccess() {
        startCountDownTimer();

        presenter.fetchBlogPosts();
    }

    private void startCountDownTimer() {
        final long adsTime = adsBean.getGmDietAds().size() * Constant.TIME_INTERVAL;
        countDownTimer = new CountDownTimer(adsTime, Constant.TIME_INTERVAL){

            @Override
            public void onTick(long l) {
                final int count = (int) (l / Constant.TIME_INTERVAL) - 1;

                RequestOptions options = new RequestOptions();
                options.centerCrop();
                Glide.with(HomeActivity.this).load(adsBean.getGmDietAds().get(count).getImageUrl()).apply(options).into(adsImage);

                adsImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(HomeActivity.this, WebActivity.class);
                        intent.putExtra(Constant.WEB_URL, adsBean.getGmDietAds().get(count).getAffiliateUrl());
                        startActivity(intent);
                    }
                });
            }

            @Override
            public void onFinish() {
                countDownTimer.cancel();
                startCountDownTimer();
            }
        };

        countDownTimer.start();
    }

    @Override
    public void fetchingAdsFailure() {
        AppUtils.dismissProgressDialog();
        Toast.makeText(this, getResources().getString(R.string.fetching_error), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void fetchingBlogPostsSuccess() {
        AppUtils.dismissProgressDialog();

        BlogRecyclerAdapter adapter = new BlogRecyclerAdapter(this, blogBean);
        blogRecyclerView.setAdapter(adapter);
        blogRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

    }

    @Override
    public void fetchingBlogPostsFailure() {
        AppUtils.dismissProgressDialog();
        Toast.makeText(this, getResources().getString(R.string.fetching_error), Toast.LENGTH_SHORT).show();
    }

//    @OnClick(R.id.options_veg)
//    public void vegOptionClicked() {
//        SharedPreferenceUtils.setUserDiet(HomeActivity.this, Constant.OPTION_VEG);
//    }
//
//    @OnClick(R.id.options_non_veg)
//    public void nonVegOptionClicked() {
//        SharedPreferenceUtils.setUserDiet(HomeActivity.this, Constant.OPTION_NON_VEG);
//    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(countDownTimer != null) {
            countDownTimer.cancel();
        }
    }
}
