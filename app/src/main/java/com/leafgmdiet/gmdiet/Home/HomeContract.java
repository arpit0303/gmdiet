package com.leafgmdiet.gmdiet.Home;

import android.content.Context;

/**
 * Created by Arpit on 26/12/17.
 */

interface HomeContract {

    public interface Presenter {
        boolean checkLoginAndUserData(Context context);

        void fetchRecommendDrinks(Context context);

        void fetchAds();

        void fetchBlogPosts();
    }

    public interface View {
        void recommendDrinksSuccess();

        void recommendDrinksFailure();

        void fetchingAdsSuccess();

        void fetchingAdsFailure();

        void fetchingBlogPostsSuccess();

        void fetchingBlogPostsFailure();
    }
}
