package com.leafgmdiet.gmdiet.exerciseFragment;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.leafgmdiet.gmdiet.R;
import com.leafgmdiet.gmdiet.model.DietModel;

/**
 * Created by Arpit on 31/01/18.
 */

public class ExerciseAdapter extends RecyclerView.Adapter<ExerciseAdapter.ExerciseViewHolder> {

    @Override
    public ExerciseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_exercise, null);
        return new ExerciseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ExerciseViewHolder holder, int position) {
        holder.exerciseTitle.setText(DietModel.EXERCISE_TITLE[position]);
        holder.exerciseImageView.setImageResource(DietModel.EXERCISE_IMAGE[position]);
    }

    @Override
    public int getItemCount() {
        return DietModel.EXERCISE_TITLE.length;
    }

    public class ExerciseViewHolder extends RecyclerView.ViewHolder {

        ImageView exerciseImageView;
        TextView exerciseTitle;

        public ExerciseViewHolder(View itemView) {
            super(itemView);

            exerciseImageView = itemView.findViewById(R.id.exercise_image);
            exerciseTitle = itemView.findViewById(R.id.exercise_title);
        }
    }
}
