package com.leafgmdiet.gmdiet.exerciseFragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.leafgmdiet.gmdiet.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Arpit on 31/01/18.
 */

public class ExerciseFragment extends Fragment {

    @BindView(R.id.exercise_recycler_view)
    RecyclerView exerciseRecyclerView;

    public static ExerciseFragment getInstance() {
        ExerciseFragment exerciseFragment = new ExerciseFragment();
        return exerciseFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_exercise, container, false);
        ButterKnife.bind(this, rootView);
        init();

        return rootView;
    }

    private void init() {
        ExerciseAdapter adapter = new ExerciseAdapter();
        exerciseRecyclerView.setAdapter(adapter);

        exerciseRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    }
}
