package com.leafgmdiet.gmdiet.drinkFragment;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.leafgmdiet.gmdiet.Beans.RecommendDrinksBean;
import com.leafgmdiet.gmdiet.R;
import com.leafgmdiet.gmdiet.utils.Constant;
import com.leafgmdiet.gmdiet.webactivity.WebActivity;

/**
 * Created by Arpit on 01/02/18.
 */

public class DrinkOptionsAdapter extends RecyclerView.Adapter<DrinkOptionsAdapter.DrinkOptionsViewHolder>{

    RecommendDrinksBean bean;
    Context context;

    public DrinkOptionsAdapter(Context context, RecommendDrinksBean bean) {
        this.context = context;
        this.bean = bean;
    }

    @Override
    public DrinkOptionsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_drink_options, null);
        return new DrinkOptionsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DrinkOptionsViewHolder holder, final int position) {
        holder.drinkTitle.setText(bean.getGmDietDrinks().get(position).getTitle());
        holder.drinkSubTitle.setText(bean.getGmDietDrinks().get(position).getSubtitle());

        Glide.with(context).load(bean.getGmDietDrinks().get(position).getImageUrl()).into(holder.drinkImage);

        holder.llDrink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, WebActivity.class);
                intent.putExtra(Constant.WEB_URL, bean.getGmDietDrinks().get(position).getAffiliateUrl());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        if(bean.getGmDietDrinks() != null) {
            return bean.getGmDietDrinks().size();
        }else{
            return 0;
        }

    }

    public class DrinkOptionsViewHolder extends RecyclerView.ViewHolder {

        ImageView drinkImage;
        TextView drinkTitle, drinkSubTitle;
        LinearLayout llDrink;

        public DrinkOptionsViewHolder(View itemView) {
            super(itemView);

            drinkImage = itemView.findViewById(R.id.suggested_drink_image);
            drinkTitle = itemView.findViewById(R.id.suggested_drink_title);
            drinkSubTitle = itemView.findViewById(R.id.suggested_drink_sub_title);
            llDrink = itemView.findViewById(R.id.ll_suggested_drink);
        }
    }
}
