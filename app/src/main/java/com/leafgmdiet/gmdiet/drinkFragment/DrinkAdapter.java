package com.leafgmdiet.gmdiet.drinkFragment;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.leafgmdiet.gmdiet.R;

/**
 * Created by Arpit on 01/02/18.
 */

public class DrinkAdapter extends RecyclerView.Adapter<DrinkAdapter.DrinkViewHolder> {

    public DrinkAdapter() {

    }

    @Override
    public DrinkViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_drink, null);
        return new DrinkViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DrinkViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 2;
    }

    public class DrinkViewHolder extends RecyclerView.ViewHolder {

        public DrinkViewHolder(View itemView) {
            super(itemView);
        }
    }
}
