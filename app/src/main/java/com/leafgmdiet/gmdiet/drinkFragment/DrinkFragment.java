package com.leafgmdiet.gmdiet.drinkFragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.leafgmdiet.gmdiet.Beans.RecommendDrinksBean;
import com.leafgmdiet.gmdiet.R;
import com.leafgmdiet.gmdiet.utils.Constant;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Arpit on 31/01/18.
 */

public class DrinkFragment extends Fragment {

    @BindView(R.id.drink_recycler_view)
    RecyclerView drinkRecyclerView;

    @BindView(R.id.drink_option_recyler_view)
    RecyclerView drinkOptionsRecylerView;

    RecommendDrinksBean bean;

    public static DrinkFragment getInstance(String data) {
        Bundle bundle = new Bundle();
        bundle.putString(Constant.INTENT_JSON_DATA, data);
        DrinkFragment drinkFragment = new DrinkFragment();
        drinkFragment.setArguments(bundle);
        return drinkFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_drink, container, false);
        ButterKnife.bind(this, rootView);
        init();

        return rootView;
    }

    private void init() {
        bean = new Gson().fromJson(getArguments().getString(Constant.INTENT_JSON_DATA), RecommendDrinksBean.class);

        DrinkAdapter adapter = new DrinkAdapter();
        drinkRecyclerView.setAdapter(adapter);
        drinkRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        DrinkOptionsAdapter drinkOptionsAdapter = new DrinkOptionsAdapter(getActivity(), bean);
        drinkOptionsRecylerView.setAdapter(drinkOptionsAdapter);
        drinkOptionsRecylerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
    }
}
